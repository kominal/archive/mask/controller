import { deleteFloatingIp } from '@kominal/lib-node-hetzner-interface';
import { Scheduler } from '@kominal/lib-node-scheduler';
import { debug, info, warn } from '@kominal/observer-node-client';
import { connections } from '../connection';
import { SessionDatabase } from '../models/session';
import { floatingIps, loadFloatingIps, loadServers, servers } from '../routes/session';

let counter = 0;

export class SessionScheduler extends Scheduler {
	async run() {
		counter++;
		if (counter <= 1) {
			debug(`Ignoring run. Grace period.`);
			return;
		}

		const serverIds = servers.filter((s) => connections.find((c) => c.nodeHostname === s.name)).map((s) => s.id);

		const date = new Date();
		date.setDate(date.getDate() - 1);

		const sessionsToDelete: string[] = [];

		SESSION: for (const session of await SessionDatabase.find({ lastActivity: { $lt: date } })) {
			const floatingIp = floatingIps.find((f) => f.ip === session.maskIp);

			if (!floatingIp) {
				sessionsToDelete.push(session._id);
				continue;
			}

			if (!floatingIp.server) {
				continue;
			}

			if (!serverIds.includes(floatingIp.server)) {
				continue;
			}

			for (const connection of connections) {
				if (connection.sessions.find((s) => s.maskIp === session.maskIp)) {
					continue SESSION;
				}
			}

			info(`Marking session(${session.group}/${session.identifier}/${session.family}) to be deleted`);
			sessionsToDelete.push(session._id);
		}

		await SessionDatabase.deleteMany({ _id: { $in: sessionsToDelete } });

		let updated = false;

		IP: for (const floatingIp of floatingIps) {
			if (floatingIp.server && !serverIds.includes(floatingIp.server)) {
				continue;
			}

			for (const connection of connections) {
				if (connection.sessions.find((s) => s.maskIp === floatingIp.ip)) {
					continue IP;
				}
			}

			if (await SessionDatabase.exists({ maskIp: floatingIp.ip })) {
				continue;
			}

			info(`Deleting floating IP ${floatingIp.ip}...`);
			try {
				await deleteFloatingIp(floatingIp.id);
			} catch (e) {
				warn(`Deleting of floating IP ${floatingIp.ip} failed: ${e.message}`);
			}
			updated = true;
		}

		if (updated) {
			await loadFloatingIps();
			await loadServers();
		}
	}
}
