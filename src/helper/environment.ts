import { getEnvironmentNumber, getEnvironmentString } from '@kominal/lib-node-environment';

export const STACK_NAME = getEnvironmentString('STACK_NAME', '');
export const LISTEN_PORT = getEnvironmentNumber('LISTEN_PORT', 1080);
