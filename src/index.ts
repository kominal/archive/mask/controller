import { ExpressRouter } from '@kominal/lib-node-express-router';
import { MongoDBInterface } from '@kominal/lib-node-mongodb-interface';
import { info, startObserver } from '@kominal/observer-node-client';
import { Connection } from './connection';
import { ipRouter } from './routes/ip';
import { loadFloatingIps, loadServers, maskRouter } from './routes/session';
import { SessionScheduler } from './scheduler/session.scheduler';

let mongoDBInterface: MongoDBInterface;
let expressRouter: ExpressRouter;

async function start() {
	startObserver();
	await loadServers();
	await loadFloatingIps();
	mongoDBInterface = new MongoDBInterface('controller');
	await mongoDBInterface.connect();
	expressRouter = new ExpressRouter({
		baseUrl: 'controller',
		healthCheck: async () => true,
		socketHandler: async (socket) => {
			new Connection(socket);
		},
		routes: [maskRouter, ipRouter],
	});
	await expressRouter.start();
	await new SessionScheduler().start(60);
}
start();

process.on('SIGTERM', async () => {
	info(`Received system signal 'SIGTERM'. Shutting down service...`);
	expressRouter.getServer()?.close();
	await mongoDBInterface.disconnect();
});
