import { Router } from '@kominal/lib-node-express-router';
import {
	assignFloatingIp,
	createFloatingIp,
	FloatingIp,
	getAllFloatingIps,
	getAllServers,
	Server,
} from '@kominal/lib-node-hetzner-interface';
import { debug, error, info, warn } from '@kominal/observer-node-client';
import AsyncLock from 'async-lock';
import { Response } from 'express';
import SSH2Promise from 'ssh2-promise';
import { Connection, connections } from '../connection';
import { LISTEN_PORT, STACK_NAME } from '../helper/environment';
import { Session, SessionDatabase } from '../models/session';

export let servers: Server[] = [];
export let floatingIps: FloatingIp[] = [];

export let configurations = new Map<number, string>();

export async function loadServers() {
	servers = await getAllServers();
}

export async function loadFloatingIps() {
	floatingIps = (await getAllFloatingIps()).filter((ip) => ip.name.startsWith(STACK_NAME));
}

export const maskRouter = new Router();

const lock = new AsyncLock();

type SessionIdentifier = {
	group: string;
	identifier: string;
	family: number;
};

maskRouter.getAsGuest<SessionIdentifier, { ip: string; port: number; maskIp: string }, never>(
	'/mask/:group/:identifier/:family',
	async (req, res) => {
		const { group, identifier, family } = req.params;

		debug(`Received request for mask with group ${group}, identifier ${identifier} and family ${family}.`);

		lock.acquire('MASK', () => handleSessionRequest({ group, identifier, family: Number.parseInt(family.toString()) }, res));
	}
);

async function handleSessionRequest(sessionIdentifier: SessionIdentifier, res: Response) {
	try {
		let floatingIp: FloatingIp | undefined;
		let session: Session | null = await SessionDatabase.findOne(sessionIdentifier);

		if (session) {
			floatingIp = floatingIps.find((f) => f.ip === session?.maskIp);
			if (!floatingIp) {
				await SessionDatabase.deleteOne(sessionIdentifier);
				session = null;
			}
		}

		if (!session) {
			debug(`Could not find session for identifier(${JSON.stringify(sessionIdentifier)})`);
			const sessionAndFloatingIp = await createSessionWithFloatingIp(sessionIdentifier);
			session = sessionAndFloatingIp.session;
			floatingIp = sessionAndFloatingIp.floatingIp;
		}

		if (!session || !floatingIp) {
			warn('No session/floatingIp found.');
			res.status(404).send();
			return;
		}

		if (!floatingIp.server) {
			const availableServers = servers
				.filter((s) => connections.find((c) => c.nodeHostname === s.name) && s.public_net.floating_ips.length < 20)
				.sort((s1, s2) => s1.public_net.floating_ips.length - s2.public_net.floating_ips.length);

			if (availableServers.length === 0) {
				warn('No available server found.');
				res.status(404).send();
				return;
			}

			const server = availableServers[0];

			await assignFloatingIp(floatingIp.id, server.id);
			await loadFloatingIps();
			await loadServers();
			floatingIp = floatingIps.find((f) => f.id === floatingIp?.id);
		}

		if (!floatingIp) {
			warn('No floatingIp found');
			res.status(404).send();
			return;
		}

		const server = servers.find((s) => s.id === floatingIp?.server);
		if (!server) {
			error('Server from floatingIp not found.');
			res.status(404).send();
			return;
		}

		const connection = connections.find((c) => c.nodeHostname === server?.name);
		if (!connection) {
			warn(`No connection found for server(${server.name})`);
			res.status(404).send();
			return;
		}

		await configureFloatingIP(connection, floatingIp);

		info(
			`Client(${sessionIdentifier.group}/${sessionIdentifier.identifier}/${sessionIdentifier.family}) is assigned FloatingIp(${floatingIp.ip}) on Proxy(${connection.nodeHostname})`
		);

		res.status(200).send({
			ip: connection.ip,
			port: LISTEN_PORT,
			maskIp: floatingIp.ip,
		});
	} catch (e) {
		res.status(500).send();
		return;
	}
}

export async function createSessionWithFloatingIp(
	sessionIdentifier: SessionIdentifier
): Promise<{ session: Session; floatingIp: FloatingIp }> {
	const floatingIp = await findFreeFloatingIp(sessionIdentifier);

	const session: Session = {
		...sessionIdentifier,
		maskIp: floatingIp.ip,
		lastActivity: new Date(),
	};

	await SessionDatabase.updateOne(sessionIdentifier, { maskIp: session.maskIp, lastActivity: session.lastActivity }, { upsert: true });

	return { session, floatingIp };
}

export async function findFreeFloatingIp(sessionIdentifier: SessionIdentifier): Promise<FloatingIp> {
	const serverIds = servers.filter((s) => connections.find((c) => c.nodeHostname === s.name)).map((s) => s.id);

	IP: for (const floatingIp of floatingIps) {
		if (floatingIp.server && !serverIds.includes(floatingIp.server)) {
			continue IP;
		}

		for (const connection of connections) {
			if (connection.sessions.find((s) => s.group === sessionIdentifier.group && s.maskIp === floatingIp.ip)) {
				continue IP;
			}
		}

		if (await SessionDatabase.exists({ group: sessionIdentifier.group, maskIp: floatingIp.ip })) {
			continue IP;
		}

		return floatingIp;
	}

	return createNewFloatingIp(sessionIdentifier.family);
}

export async function createNewFloatingIp(family: number): Promise<FloatingIp> {
	debug(`Creating new floating ip.`);
	const datacenters = ['nbg1', 'fsn1', 'hel1'];
	const datacenter = datacenters[Math.floor(Math.random() * datacenters.length)];
	const floatingIp = await createFloatingIp(family === 4 ? 'ipv4' : 'ipv6', datacenter, `${STACK_NAME}_${Date.now()}`);
	await loadFloatingIps();
	return floatingIp;
}

export async function configureFloatingIP(connection: Connection, floatingIp: FloatingIp) {
	if (connection.configuredFloatingIps.includes(floatingIp.id)) {
		return;
	}
	const ssh = new SSH2Promise({
		host: connection.ip,
		username: 'root',
		identity: '/opt/manager/id_rsa',
	});
	await ssh.connect();
	try {
		if (floatingIp.type === 'ipv4') {
			await ssh.exec(`ip addr add ${floatingIp.ip}/32 dev eth0`);
		}
	} catch (e) {}
	await ssh.close();
	connection.configuredFloatingIps.push(floatingIp.id);
}

maskRouter.deleteAsGuest<{ group: string }, never, never, never>('/session/:group', async (req, res) => {
	await SessionDatabase.deleteMany(req.params);
	res.status(200).send();
});

maskRouter.deleteAsGuest<SessionIdentifier, never, never, never>('/session/:group/:identifier/:family', async (req, res) => {
	await SessionDatabase.deleteMany(req.params);
	res.status(200).send();
});
