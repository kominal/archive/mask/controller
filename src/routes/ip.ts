import { Router } from '@kominal/lib-node-express-router';
import { FloatingIp } from '@kominal/lib-node-hetzner-interface';
import { error, warn } from '@kominal/observer-node-client';
import { connections } from '../connection';
import { LISTEN_PORT } from '../helper/environment';
import { configureFloatingIP, floatingIps, servers } from './session';

export const ipRouter = new Router();

ipRouter.getAsService<never, FloatingIp[], never>('/ips', async (req, res) => {
	return { statusCode: 200, responseBody: floatingIps };
});

ipRouter.getAsGuest<{ ip: string }, { ip: string; port: number; maskIp: string }, never>('/ips/:ip', async (req, res) => {
	try {
		const floatingIp = floatingIps.find((f) => f.ip === req.params.ip);

		if (!floatingIp) {
			warn('No floatingIp found');
			res.status(404).send();
			return;
		}

		const server = servers.find((s) => s.id === floatingIp?.server);
		if (!server) {
			error('Server from floatingIp not found.');
			res.status(404).send();
			return;
		}

		const connection = connections.find((c) => c.nodeHostname === server?.name);
		if (!connection) {
			warn(`No connection found for server(${server.name})`);
			res.status(404).send();
			return;
		}

		await configureFloatingIP(connection, floatingIp);

		res.status(200).send({
			ip: connection.ip,
			port: LISTEN_PORT,
			maskIp: floatingIp.ip,
		});
	} catch (e) {
		res.status(500).send();
		return;
	}
});
